#include<cmw/Buffer.hh>
using namespace ::cmw;
#include"zz.frontMiddle.hh"
#include<stdio.h>
#include<stdlib.h>//exit

template<class...T>void leave (char const* fmt,T...t)noexcept{
  ::fprintf(stderr,fmt,t...);
  ::exit(EXIT_FAILURE);
}

int main (int ac,char** av)try{
  if(ac<3||ac>5)
    leave("Usage: genz account-num mdl-file-path [node] [port]\n");
  winStart();
  getaddrinfoWrapper res(ac<4?
#ifdef __linux__
                         "127.0.0.1":av[3]
#else
                         "::1":av[3]
#endif
                         ,ac<5?"55555":av[4],SOCK_DGRAM);
  BufferStack<SameFormat> buf(res.getSock());

  ::frontMiddle::Marshal(buf,marshallingInt(av[1]),av[2]);
  for(int tm=8;tm<17;tm+=8){
    buf.Send(res()->ai_addr,res()->ai_addrlen);
    setRcvTimeout(buf.sock_,tm);
    if(buf.GetPacket()){
      if(giveBool(buf))::exit(EXIT_SUCCESS);
      leave("cmwA:%s\n",nullTerminate(giveStringView(buf)).data());
    }
  }
  leave("No reply received.  Is the cmwA running?\n");
}catch(::std::exception const& e){leave(e.what());}
